<?php

return array(
	'dimensions'=>
		array(
			'web_thumb'=> array('width'=>150, 'height'=>150, 'thumb'=>true),
			'tablet_full' => array('width'=>1536, 'height'=>2048),
			'tablet_thumb' => array('width'=>150, 'height'=>150, 'thumb'=>true),
			'phone_full' => array('width'=>1280, 'height'=>2272),
			'phone_thumb' => array('width'=>90, 'height'=>90, 'thumb'=>true),
		),
		
	'type'=>
		array(
			'original',
			'web_thumb',
			'tablet_full',
			'tablet_thumb',
			'phone_full',
			'phone_thumb'
		)		
);