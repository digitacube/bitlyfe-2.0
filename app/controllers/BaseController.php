<?php

class BaseController extends Controller {
	protected $status_code = 200;
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	protected function json($array)
	{
		return Response::JSON($array, $this->status_code);
	}
}