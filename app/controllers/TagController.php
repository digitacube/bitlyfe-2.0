<?php

class TagController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$validator = Validator::make(
			Input::all(),
			array(
			'tag_text' => 'required'
			)
		);
		
		if (!$validator->fails())
		{
			try
			{
				$tag = Tag::firstOrCreate(
					array(
					'user_id'=>Auth::user()->user_id,
					'tag_text'=>Input::get('tag_text');
					)
				);
			
			$response['success'] = true;
			$response['tag'] = $tag;
			}
			catch(Exception $e)
			{
				unset($response);
				$response['success'] = false;
				$response['error'] = $e;
				$this->status_code = 503;
			}
		}else
		{
			unset($response);
			$response['success'] = false;
			$response['error'] = $validator->failed();
			$this->status_code = 400;
		}
		
		return json($response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}