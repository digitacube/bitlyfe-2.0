<?php

class TimelineController extends \BaseController {
	protected $layout = 'layout.master';
	
	protected $validation_rules = 
	array(
		'event_name'=>'required',
		'event_start'=>'required|date',
		'event_end'=>'required_without:event_ongoing|date_after_field:event_start'
	);
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$data['events'] = Timeline::all();
		$this->layout->content = View::make('timeline/index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = array();
		//code to insert to database here
		
		$this->layout->content = View::make('timeline.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		if (Request::isMethod('post'))
		{
			$data = Input::all();
			$validator = Validator::make(
				$data,
				$this->validation_rules
			);
			
			if (!$validator->fails())
			{
				$data['event_start'] = date('Y-m-d H:i:s',strtotime($data['event_start']));
				if (isset($data['event_end']))
				{
					$data['event_end'] = date('Y-m-d H:i:s',strtotime($data['event_end']));
				}
				
				$data['user_id'] = Auth::user()->user_id;
				
				//remove fields
				unset($data['event_ongoing']);
				
				$event = Timeline::create($data);
								
				return Redirect::to('timeline')->with('success_messages', 'alert.timeline.create_success');
			}
			else
			{
				$data['messages'] = $validator->messages();
				Input::flash();
				return Redirect::to('timeline/create')->withErrors($validator->messages());
			}
			
			return Redirect::to('timeline/create');
		}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$data['title'] = 'edit_event';
		$data['event'] = Timeline::find($id);
		if (!$data['event'])
		{
			return Redirect::to('timeline')->with('message', 'Failed to load event!');
		}
		
		$data['event']->event_start = date('Y-m-d',strtotime($data['event']->event_start));
		
		if ($data['event']->event_end != 0)
		{
			$data['event']->event_end = date('Y-m-d',strtotime($data['event']->event_end));
		}
		$data['photos'] = $data['event']->photo()->get();
		$this->layout->content = View::make('timeline.create', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		//submission part
		if (Request::isMethod('post'))
		{
			$data = Input::all();
			$validator = Validator::make(
				$data,
				$this->validation_rules
			);
			
			
			if (!$validator->fails())
			{
				$data['event_start'] = date('Y-m-d H:i:s',strtotime($data['event_start']));
				if (isset($data['event_end']))
				{
					$data['event_end'] = date('Y-m-d H:i:s',strtotime($data['event_end']));
				}
				
				$data['user_id'] = Auth::user()->user_id;
				
				//remove fields
				unset($data['event_ongoing']);
				
				
				$event = Timeline::find($id);
				$result = $event->update($data);
				
				if (Input::hasFile('photo'))
				{
					$this->upload_images($result->event_id);
				}
				
				return Redirect::to('timeline')->with('success_messages', 'alert.timeline.update_success');

			}
			else
			{
				$data['messages'] = $validator->messages();
				Input::flash();
				
				return Redirect::to('timeline/create')->withErrors($validator->messages());
			}
			
			return Redirect::to('timeline/create');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	
	//save images, return an array of file paths for each dimension saved
	//return false if failed
	public function upload_images()
	{
		if (Request::isMethod('post'))
		{
			$event = Input::all();
			$validator = Validator::make(
				$event,
				array('event_id'=>'required')
			);
			
			if ($validator->fails())
			{
				$timeline = Timeline::create(array());
				$event_id = $timeline->event_id;
			}else
			{
				$event_id = $event->event_id;
			}
			
			
		}
	
		if (Input::hasFile('photo'))
		{
			$photo_storage = array();
			//for each photo uploaded
			foreach (Input::file('photo') as $photo)
			{
				
				$img =  Image::make($photo->getRealPath());
				
				$rotations = Image_autorotate::get_rotation($photo->getRealPath());
				
				foreach ($rotations as $rotation)
				{
					if (!is_int($rotation))
					{
						$img->flip($rotation);
					}else
					{
						$img->rotate($rotation);
					}
					
				}
				if (Input::get('photo_caption'))
				{
					$data['photo_caption'] = Input::get('photo_caption');
				}
				$data['uploaded_by'] = Auth::user()->user_id;
				$data['event_id'] = $event_id;
				
				$photo_data = Photo::create($data);
				
				$saved_path = 'assets/img/'.uniqid().'.'.$photo->getClientOriginalExtension();
				$img->save($saved_path);
				
				$storage_data['storage_type'] = 0; //original image is 0
				$storage_data['storage_path'] = $saved_path;
				$storage_data['photo_id'] = $photo_data->photo_id;
				
				Photo_storage::create($storage_data);
				
				$photo_storage[] = $storage_data;
				
				//save the rotation
				$img->backup();
				//foreach photo dimension
				//specified in config/photos
				foreach (Config::get('photos.dimensions') as $type => $dimensions)
				{
					
					//try
					//{
						if (isset($dimensions['thumb']))
						{
							if ($img->width > $img->height)
							{
								$img->resize(null, $dimensions['height'], true, false);
								$pos_x = ($img->width - $dimensions['width'])/2;
								
								$img->crop($dimensions['width'], $dimensions['height'], $pos_x);
							}
							else
							{
								$img->resize($dimensions['width'], null, true, false);
								$pos_y = ($img->height - $dimensions['height'])/2;
								
								$img->crop($dimensions['width'], $dimensions['height'], null, $pos_y);
							}
						}else
						{
							if ($img->width > $img->height)
							{
								$img->resize($dimensions['width'], null, true, false);
							}
							else 
							{
								$img->resize(null, $dimensions['height'], true, false);
							}
						}
						$saved_path = 'assets/img/'.uniqid().$photo->getClientOriginalExtension();
						$img->save($saved_path);
						
						$storage_data['storage_type'] = array_search($type, Config::get('photos.type'));
						$storage_data['storage_path'] = $saved_path;
						$storage_data['photo_id'] = $photo_data->photo_id;
					
						Photo_storage::create($storage_data);
					
						$photo_storage[] = $storage_data;
						//reset and do it again
						$img->reset();
					//}
					//catch(Exception $e)
					//{
					//	return json_encode(array('success'=>false));
					//}
				}
				
				
				
				
			}//end foreach photos
			
			
		}//endif has file
		return Redirect::to('timeline/edit/'.$event_id);
		//return json_encode(array('success'=>true));
	}
	
	//tag the event
	public function tagEvent($id)
	{
		
	}
	
	public function getPhotos($id)
	{
		$validator = Validator::make(
			array(
				'event_id' => $id
			),
			array(
				'event_id'=>'required|exists:events,event_id'
			)
		);
		
		if (!$validator->fails())
		{
			try
			{
				$event = Timeline::findOrFail($id);
				$photos = $event->photos;
				$response['success'] = true;
				$response['event'] = $event->toArray();
				$response['photos'] = $photos->toArray();
				
				foreach ($photos as $key => $photo)
				{
					$storage = $photo->storage;
					$response['photos'][$key]['storage'] = $storage->toArray();
				}
			}
			catch(Exception $e)
			{
				unset($response);
				$response['success'] = false;
				$response['error'] = $e;
				$this->status_code = 503;
			}
		}else
		{
			unset($response);
			$response['success'] = false;
			$response['error'] = $validator->failed();
			$this->status_code = 400;
		}
		
		return $this->json($response);
		
	}
}