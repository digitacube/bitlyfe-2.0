<?php

class UserController extends \BaseController {
	
	protected $layout = 'layout.master';
	
    /**
     * Login function
     *
     * @return Redirect
     */
    public function login()
    {
        $login_vars = Input::only('user_email','user_password');
        if (Auth::attempt(array('user_email' => $login_vars['user_email'], 'password' => $login_vars['user_password'])))
        {
        	return Redirect::intended('timeline');
        }
        else
        {
        	Session::flash('login_error','login_failed');
			return Redirect::to('login');
        }
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = array();
		$this->layout->content = View::make('users.registration', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return "test";
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
