<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('events', function($table)
		{
			$table->bigIncrements('event_id');
			$table->string('event_name', 250);
			$table->timestamp('event_start');
			$table->timestamp('event_end');
			$table->text('event_description');
			$table->bigInteger('user_id');
			$table->softDeletes();
			$table->timestamps();
			$table->index('event_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('events');
	}

}
