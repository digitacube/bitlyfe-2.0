<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('users', function($table)
		{
			$table->bigIncrements('user_id');
			$table->string('user_email', 100)->unique();
			$table->string('user_password', 100);
			$table->string('user_first_name', 100)->index();
			$table->string('user_last_name', 100)->index();
            $table->boolean('active')->default(1);
			$table->dateTime('user_date_of_birth');
			$table->softDeletes();
            $table->timestamps();
			$table->index(array('user_first_name', 'user_last_name'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
	}

}
