<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('photos', function($table)
		{
			$table->bigIncrements('photo_id');
			$table->text('photo_path', 250);
			$table->timestamp('photo_date');
			$table->text('photo_caption');
			$table->unsignedBigInteger('uploaded_by');
			$table->unsignedBigInteger('event_id');
			$table->foreign('event_id')->references('event_id')->on('events');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('photos');
	}

}
