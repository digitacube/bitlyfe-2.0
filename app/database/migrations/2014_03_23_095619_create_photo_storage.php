<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoStorage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photo_storage', function(Blueprint $table)
		{
			$table->increments('storage_id');
			$table->timestamps();
			$table->unsignedBigInteger('photo_id');
			$table->integer('storage_type');
			$table->text('storage_path');
			$table->softDeletes();
			$table->foreign('photo_id')->references('photo_id')->on('photos');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photo_storage');
	}

}
