<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_tag', function(Blueprint $table) {
			$table->unsignedBigInteger('event_id');
			$table->unsignedBigInteger('tag_id');
			
			$table->foreign('event_id')->references('event_id')->on('events');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_tag');
	}

}
