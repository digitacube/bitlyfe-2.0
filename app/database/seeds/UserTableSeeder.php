<?php

class UserTableSeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();

		Eloquent::unguard();
        User::create(
            array(
                'user_email' => 'test@bar.com',
                'user_password' => Hash::make('test'),
                'user_first_name' => 'Foo',
                'user_last_name' => 'Bar',
                'user_date_of_birth' => '1990/06/05',
                'active' => 1,
            )
        );
    }
}
?>