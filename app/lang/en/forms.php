<?php

return array(
	'labels' => array(
		//login form
		'user_email'=>'Email Address',
		'user_password'=>'Password',
		//registration form
		'user_name' => 'Username',
		'user_password'=>'Password',
		'user_re_password'=>'Confirm Password',
		'user_re_email'=>'Retype Email Address',
		//events form
		'event_name' => 'Event Name',
		'event_start'=>'Event Start Date',
		'event_ongoing'=>'Ongoing event',
		'event_end'=>'Event End Date',
		'event_description'=>'Event Caption'
		
	),
	'placeholders' => array(
		'event_name' => 'eg. Awesome Lunch!',
		'event_start'=>'eg. 2014-01-01',
		'event_end'=>'eg. 2014-01-02'
	),
	'buttons'=>array(
		'login'=>'Login'
	)
);