<?php  
/**
* @file app/libraries/Image_autorotate.php
*/
class Image_autorotate
{
	function __construct($params = NULL) 
	{
	
	}
	
	public static function get_rotation($filepath = NULL)
	{	
		if ($filepath===NULL || empty($filepath)) return FALSE;
		
		$exif = @exif_read_data($filepath);
		//print_r($exif);
		if (empty($exif['Orientation'])) return FALSE;
		
				
		$oris = array();
		
		switch($exif['Orientation'])
		{
		        case 1: // no need to perform any changes
		        break;
		
		        case 2: // horizontal flip
							$oris[] = 'h';
		        break;
		                                
		        case 3: // 180 rotate left
		        	$oris[] = 180;
		        break;
		                    
		        case 4: // vertical flip
		        	$oris[] = 'v';
		        break;
		                
		        case 5: // vertical flip + 90 rotate right
		        	$oris[] = 'v';
							$oris[] = 270;
		        break;
		                
		        case 6: // 90 rotate right
		        	$oris[] = 270;
		        break;
		                
		        case 7: // horizontal flip + 90 rotate right
		        	$oris[] = 'h';
							$oris[] = 270;
		        break;
		                
		        case 8: // 90 rotate left
		        	$oris[] = 90;
		        break;
				
			default: break;
		}
		return $oris;		
	}
}