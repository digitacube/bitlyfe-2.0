<?php

class Photo extends Eloquent
{
	protected $table = 'photos';

	protected $guarded = array('photo_id');

	protected $primaryKey = 'photo_id';

	protected $softDelete = true;
	
	public function event()
	{
		return $this->belongsTo('Timeline', 'event_id');
	}
	
	public function storage()
	{
		return $this->hasMany('Photo_storage', 'photo_id');
	}
}
