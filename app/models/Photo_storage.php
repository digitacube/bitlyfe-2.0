<?php

class Photo_storage extends Eloquent
{
	protected $table = 'photo_storage';

	protected $guarded = array('storage_id');

	protected $primaryKey = 'storage_id';

	protected $softDelete = true;
	
	public function photo()
	{
		return $this->belongsTo('Photo', 'photo_id');
	}
}
