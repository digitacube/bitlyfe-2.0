<?php

class Tag extends Eloquent
{
	protected $table = 'tags';

	protected $guarded = array('tag_id');

	protected $primaryKey = 'tag_id';

	protected $softDelete = true;
	
	public function user()
	{
		return $this->belongsTo('users', 'user_id');
	}
}
