<?php

class Timeline extends Eloquent
{
	protected $table = 'events';
	
	protected $guarded = array('event_id');
	
	protected $primaryKey = 'event_id';
	
	protected $softDelete = true;
	
	public function photos()
	{
		return $this->hasMany('Photo', 'event_id');
	}
}