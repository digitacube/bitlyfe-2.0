<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//login
Route::get('login', function()
{
	View::name('layout.master', 'layout');
	$layout = View::of('layout');
	return $layout->nest('content', 'login');
});
Route::post('login', 'UserController@login');

Route::group(array('before'=>'login'), function()
{
	//first page
	Route::get('/', 'HomeController@showWelcome');
	Route::get('home','HomeController@showWelcome');

	//timeline routes
	Route::get('timeline', 'TimelineController@index');
	Route::get('timeline/create', 'TimelineController@create');
	Route::post('timeline/create', 'TimelineController@store');
	Route::get('timeline/edit/{id}', 'TimelineController@edit');
	Route::post('timeline/edit/{id}', 'TimelineController@update');
	Route::post('timeline/upload', 'TimelineController@upload_images');
	
	
});

Route::get('timeline/photos/{id}', 'TimelineController@getPhotos');

//users routes
Route::resource('users','UserController');

Route::filter('login', function()
{
	if (!Auth::check())
	{
		return Redirect::guest('login')->with('login_error','login_required');
	}
});

