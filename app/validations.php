<?php

Validator::extend('date_after_field', 'CoreValidator@DateAfterField');
Validator::extend('date_before_field', 'CoreValidator@DateBeforeField');
Validator::resolver(function($translator, $data, $rules, $messages)
{
    return new CoreValidator($translator, $data, $rules, $messages);
});
