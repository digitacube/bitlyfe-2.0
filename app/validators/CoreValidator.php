<?php

class CoreValidator extends Illuminate\Validation\Validator 
{

	/**
	* Validate date is after a certain fields
	*	
	*	@param string $attribute: name of the field being validated
	*	@param mixed $value: value of the field
	* @param array $params: name of input field matched
	*/	
  public function validateDateAfterField($attribute, $value, $params)
  {
  	return strtotime($value) > strtotime($this->data[$params[0]]);
  }
  
  
  public function replaceDateAfterField($message, $attribute, $rule, $parameters)
  {
	  return str_ireplace(":before", $this->getAttribute($parameters[0]), $message);
  }
  
  
  /**
	* Validate date is before a certain fields
	*	
	*	@param string $attribute: name of the field being validated
	*	@param mixed $value: value of the field
	* @param array $params: name of input field being matched
	*/	
  public function validateDateBeforeField($attribute, $value, $params)
  {
	  return strtotime($value) < strtotime($this->data[$params[0]]);

  }
  
  public function replaceDateBeforeField($attribute, $value, $params)
  {
	  return str_ireplace(":after", $this->getAttribute($parameters[0]), $message);
  }
}
