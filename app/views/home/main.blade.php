@section('content')
	<div id="main-content" class="col-md-9 btm-padding">
		<div id="hashtag-tiles" class="col-md-12 r-fluid" style="">
			<ul>
				<li>#hashtag</li>
				<li>#home</li>
				<li>#baby</li>
			</ul>
		</div>
		<div id="home-tiles" class="row-fluid">
			
		</div>
		
	</div>
	<div id="events-timeline" class="col-md-3" style="">
		<h3>Events Timeline</h3>
		@foreach ($events as $event)
			<div class="event-wrapper-box" data-id="{{$event->event_id}}">
				<div class="event-wrapper">
					<img src="{{asset('assets/img/test/antimage.jpg')}}"/>
					<div class="event-details">
						<div class="title">
							@if(!empty($event->event_name))
								{{$event->event_name}}
							@else
								<i>No Title</i>
							@endif
						</div>
						<div class="description">
							{{date("d M Y",strtotime($event->event_start))}} - {{date("d M Y",strtotime($event->event_start))}}
							<span class="title"></span><br/>
							<span>@if(!empty($event->event_description)) ({{$event->event_description}} @else <i>No Description</i> @endif</span><br/>
							<a href="{{ url('timeline/edit', array($event->event_id )) }}">Edit</a>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
	<?=HTML::script('assets/js/homepage.js')?>
	<?=HTML::script('assets/js/imagesloaded.min.js')?>
	<?=HTML::script('assets/js/masonry.min.js')?>
@stop