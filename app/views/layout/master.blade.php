<html>
	<head>
		<?=HTML::style('assets/css/bootstrap.min.css')?>
		<?=HTML::style('http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css')?>
		<?=HTML::style('assets/css/style.css')?>
		@yield('css')
		<?=HTML::script('http://code.jquery.com/jquery-1.11.0.min.js')?>
		<?=HTML::script('assets/js/bootstrap.min.js')?>
		<?=HTML::script('http://code.jquery.com/ui/1.10.3/jquery-ui.js')?>
		<?=HTML::script('assets/js/common.js')?>
		@yield('js')
		
		<title>@yield('title', 'Bitlyfe')</title>
	</head>

	<body>
        @include('layout.menubar')
        <div id="master-wrapper" class="container-fluid">
            <div>
                @yield('content', 'Nothing here')
            </div>
        </div>
	</body>
</html>
