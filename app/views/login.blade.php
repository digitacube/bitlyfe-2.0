
@section('content')
	<?php
		if (Session::has('login_error')):
	?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?=Lang::get('alerts.login_error.'.Session::get('login_error'))?>

		</div>
	<?php
		endif;
	?>

	<?=Form::open(array('class'=>'form-horizontal'))?>
		<legend>Login</legend>
		<!-- User Email -->
		<div class="form-group">
		<?=Form::label('user_email', Lang::get('forms.labels.user_email'), array('class'=>'col-lg-2 control-label'))?>
			<div class="col-lg-4">
				<?=Form::input('email','user_email', null, array('class'=>'form-control'))?>
			</div>
		</div>

		<!-- User Password -->
		<div class="form-group">
		<?=Form::label('user_password', Lang::get('forms.labels.user_password'), array('class'=>'col-lg-2 control-label'))?>
			<div class="col-lg-4">
				<?=Form::input('password','user_password', null, array('class'=>'form-control'))?>
			</div>
		</div>

		<!-- Submit Button -->
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-4">
				<?=Form::submit(Lang::get('forms.buttons.login'), array('class'=>'btn btn-primary'))?>
			</div>
		</div>
	<?=Form::close()?>
@stop
