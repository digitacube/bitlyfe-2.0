@extends('layout.master')

@section('title') 
	<?=isset($data['title'])?Lang::get('titles.'.$data['title']):Lang::get('titles.create_event')?> 
@stop

@section('js')
	<?=HTML::script('assets/js/event_form.js')?>	
@stop

@section('content')
	<h1>Hello!</h1>
	
	<?=Form::model(isset($event)?$event:null, array('class'=>'form-horizontal','role'=>'form'))?>
	<!-- Event Name -->
	<div class="form-group">
		<?=Form::label('event_name', Lang::get('forms.labels.event_name'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::text('event_name', null, array('placeholder'=> Lang::get('forms.placeholders.event_name'), 'class'=>'form-control'))?>
			<?php
				//error message
				if (isset($errors) && $errors->has('event_name')):
			?>
			<p class="bg-danger">
				<?php
					foreach($errors->get('event_name') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>
		</div>
	</div>
	
	<!-- Event Start Date -->
	<div class="form-group">
		<?=Form::label('event_start', Lang::get('forms.labels.event_start'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::text('event_start', null, array('placeholder'=> Lang::get('forms.placeholders.event_start'), 'class'=>'form-control date-picker'))?>
			<?=Form::checkbox('event_ongoing', '1', (isset($event['event_end'])&&$event['event_end']==0)?true:false)?> <?=Lang::get('forms.labels.event_ongoing')?>
			<?php
				//error message
				if (isset($errors) && $errors->has('event_start')):
			?>
			<p class="bg-danger">
				<?php
					foreach($errors->get('event_start') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>
		</div>
	</div>
	
	<!-- Event End Date -->
	<div class="form-group">
		<?=Form::label('event_end', Lang::get('forms.labels.event_end'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::text('event_end', null, array('placeholder'=>Lang::get('forms.placeholders.event_end'), 'class'=>'form-control date-picker'))?>
			<?php
			//error message
			if (isset($errors) && $errors->has('event_end')):
			?>
			<p class="bg-danger">
				<?php
					foreach($errors->get('event_end') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>	
		</div>
	</div>
	
	<!-- Event Descriptions -->
	<div class="form-group">
		<?=Form::label('event_description', Lang::get('forms.labels.event_description'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::textarea('event_description', null, array('class'=>'form-control', 'rows'=>4))?>
		</div>
	</div>
		
	<!-- Submit button -->
	<div class="form-group">
	<div class="col-lg-offset-2 col-lg-4">
		<?=Form::submit('Submit', array('class'=>'btn btn-primary'))?>
	</div>
	</div>
	<?=Form::close()?>
@stop