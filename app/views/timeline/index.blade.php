@section('js')
	<?=HTML::script('assets/js/jquery.fileupload.js')?>
	<?=HTML::script('assets/js/jquery.iframe-transport.js')?>	
	<?=HTML::script('assets/js/fileupload.js')?>
@stop

@section('css')
	<?=HTML::style('assets/css/jquery.fileupload.css')?>
@stop

@section('content')
<?=Form::open(array('action'=>'TimelineController@upload_images','files'=>true))?>
	<?=Form::file('photo[]',  array('multiple'=>true, 'class'=>'fileupload', 'accept'=>'image/*'))?>
	<?=Form::submit('Submit')?>
<?=Form::close()?>
@stop