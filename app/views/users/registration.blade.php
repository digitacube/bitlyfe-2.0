@section('title') 
	<?=isset($data['title'])?Lang::get('titles.'.$data['title']):Lang::get('titles.create_event')?> 
@stop


@section('content')
	<?=Form::model(isset($user)?$user:null, array('route'=>'users.store','class'=>'form-horizontal','role'=>'form'))?>
	<!-- User Name -->
	<div class="form-group">
		<?=Form::label('user_name', Lang::get('forms.labels.user_name'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::text('user_name', null, array('class'=>'form-control'))?>
			<?php
				//error message
				if (isset($messages) && $messages->has('user_name')):
			?>
			<p class="bg-danger">
				<?php
					foreach($messages->get('user_name') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>
		</div>
	</div>
	
	<!-- Password -->
	<div class="form-group">
		<?=Form::label('user_password', Lang::get('forms.labels.user_password'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::password('user_password', null, array('class'=>'form-control date-picker'))?>			
			<?php
				//error message
				if (isset($messages) && $messages->has('user_password')):
			?>
			<p class="bg-danger">
				<?php
					foreach($messages->get('user_password') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>
		</div>
	</div>
	
	<!-- Re Password -->
	<div class="form-group">
		<?=Form::label('user_re_password', Lang::get('forms.labels.user_re_password'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::password('user_re_password', null, array('class'=>'form-control date-picker'))?>			
			<?php
				//error message
				if (isset($messages) && $messages->has('user_re_password')):
			?>
			<p class="bg-danger">
				<?php
					foreach($messages->get('user_re_password') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>
		</div>
	</div>
	
	<!-- Email -->
	<div class="form-group">
		<?=Form::label('user_email', Lang::get('forms.labels.user_email'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::email('user_email', null, array('class'=>'form-control'))?>
			<?php
			//error message
			if (isset($messages) && $messages->has('user_email')):
			?>
			<p class="bg-danger">
				<?php
					foreach($messages->get('user_email') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>	
		</div>
	</div>
	
	<!-- Re Email -->
	<div class="form-group">
		<?=Form::label('user_re_email', Lang::get('forms.labels.user_re_email'), array('class'=>'col-lg-2 control-label'))?>
		<div class="col-lg-4">
			<?=Form::email('user_re_email', null, array('class'=>'form-control'))?>
			<?php
			//error message
			if (isset($messages) && $messages->has('user_re_email')):
			?>
			<p class="bg-danger">
				<?php
					foreach($messages->get('user_re_email') as $message):
						echo '<small>'.$message.'</small><br>';
					endforeach;
				?>
			</p>
			<?php
				endif;
			?>	
		</div>
	</div>
	
	<!-- Submit button -->
	<div class="form-group">
	<div class="col-lg-offset-2 col-lg-4">
		<?=Form::submit('Submit', array('class'=>'btn btn-primary'))?>
	</div>
	</div>
	<?=Form::close()?>
@stop