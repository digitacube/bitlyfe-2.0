$(document).ready(function()
{
	$('.fileupload').change(function()
	{
		console.log($(this)[0].files);
		if ($(this)[0].files)
		{
			
			$.each($(this)[0].files, function()
			{
				var img = document.createElement('img');
				var reader = new FileReader();

				reader.readAsDataURL($(this)[0]);

				reader.onload = function()
				{
					$(img).attr('src',reader.result);
				}
				
				$('body').append(img);
			});
		}
	});
	
	/*$('.fileupload').fileupload(
	{
		url: $(this).data('url'),
		dataType: 'json',
		autoUpload: false,
		add: function(e, data)
		{
			$.each(data.files, function()
			{
				console.log($(this));
				
				var img = document.createElement('img');
				var reader = new FileReader();

				reader.readAsDataURL($(this)[0]);

				reader.onload = function()
				{
					$(img).attr('src',reader.result);
				}
				
				$('body').append(img);

			})
		}
	})*/
});