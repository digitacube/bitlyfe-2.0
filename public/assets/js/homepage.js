$(document).ready(function(){
	$('.event-wrapper-box').on('click', function(){
		var id = $(this).attr('data-id');
		
		var url = "timeline/photos/"+id;
		$.ajax({
			type: "GET",
			url: url,
			success:function(response){
				//console.log(response);
				var success = response.success;
				$('#home-tiles').empty();
				if(success){
					$.each(response.photos, function(index, photo){
						//console.log(photo);
						$('#home-tiles').append('<div class="col-xs-3 item"><img src="'+photo.storage[0].storage_path+'" /></div>');
					});
				}
				var container = $('#home-tiles');
				imagesLoaded(container, function(){
					// initialize
					container.masonry({
					  columnWidth: '.col-xs-3',
					  itemSelector: '.item'
					});
				});
			},
		});
	});
});